class Task < ApplicationRecord
  has_and_belongs_to_many :tags, join_table: :tasks_tags

  validates :title, length: { in: 2..128 }
  validates :title, uniqueness: { case_sensitive: false }

  # TODO
  # validates :tags, uniqueness: true
end
