class Tag < ApplicationRecord
  has_and_belongs_to_many :tasks, join_table: :tasks_tags

  validates :title, length: { in: 2..32 }
  validates :title, uniqueness: { case_sensitive: false }
end
