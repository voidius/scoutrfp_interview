class Api::V1::TasksController < ApplicationController
  before_action :validate_requested_ids, only: :update
  before_action :set_task, only: :update

  def index
    @tasks = Task.all.includes(:tags).order(:id, 'tags.id')
    render json: @tasks, include: %i(tags)
  end

  def create
    @task = Task.new(task_attributes)

    if @task.save
      render json: @task, include: %i(tags)
    else
      render json: {errors: @task.errors}, status: :unprocessable_entity
    end
  end

  def update
    ActiveRecord::Base.transaction do
      raise 'Task update failed' unless @task.update(task_attributes)
      raise 'Task tags deletion failed' unless @task.tags.delete(@task.tags - tags_for_update)
      raise 'Task tags appending failed' unless @task.tags.append(tags_for_update - @task.tags)
    end

    render json: @task, include: %i(tags)
  rescue => e
    render json: {errors: @task.errors, details: e.message}, status: :unprocessable_entity
  end

  private def task_attributes
    params.require(:data)
      .require(:attributes)
      .permit(:title)
  end

  private def task_attributes_with_tags
    params.require(:data)
      .require(:attributes)
      .permit(:title, tags: [])
  end

  private def validate_requested_ids
    unless params[:id] == params[:data].try(:[], :id)
      render json: {errors: {id: 'ids mismatch error'}}, status: :unprocessable_entity
    end
  end

  private def set_task
    @task = Task.find_by(id: params[:id])

    if @task.blank?
      render json: {errors: {id: 'not found'}}, status: :unprocessable_entity
    end
  end

  private def tags_for_update
    @tags_for_update ||= if task_attributes_with_tags[:tags].present?
      task_attributes_with_tags[:tags].inject([]) do |memo, tag_name|
        memo << Tag.find_or_create_by(title: tag_name)
        memo
      end.uniq
    else
      []
    end
  end
end
