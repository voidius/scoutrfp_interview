class Api::V1::TagsController < ApplicationController
  before_action :validate_requested_ids, only: :update
  before_action :set_tag, only: :update

  def index
    @tags = Tag.all.order(:id)
    render json: @tags
  end

  def create
    @tag = Tag.new(tag_attributes)

    if @tag.save
      render json: @tag
    else
      render json: {errors: @tag.errors}, status: :unprocessable_entity
    end
  end

  def update
    if @tag.update(tag_attributes)
      render json: @tag
    else
      render json: {errors: @tag.errors}, status: :unprocessable_entity
    end
  end

  private def tag_attributes
    params.require(:data)
      .require(:attributes)
      .permit(:title)
  end

  # TODO: write specs
  private def validate_requested_ids
    unless params[:id] == params[:data].try(:[], :id)
      render json: {errors: {id: 'ids mismatch error'}}, status: :unprocessable_entity
    end
  end

  # TODO: write specs
  private def set_tag
    @tag = Tag.find_by(id: params[:id])

    if @tag.blank?
      render json: {errors: {id: 'not found'}}, status: :unprocessable_entity
    end
  end
end
