module ControllerHelpers
  def build_tag_params(id: 'undefined', type: 'undefined', title: 'Default title')
    {
      data: {
        type: type,
        id: id,
        attributes: {
          title: title,
        },
      },
      format: :json,
    }
  end

  def build_task_params(id: 'undefined', type: 'undefined', title: 'Default title', tags: [])
    params = {
      data: {
        type: type,
        id: id,
        attributes: {
          title: title,
        },
      },
      format: :json,
    }

    if tags.present?
      params[:data][:attributes][:tags] = tags
    end

    params
  end
end
