FactoryBot.define do
  factory :tag do
    sequence(:title) { |n| "Tag_#{n}" }
  end
end
