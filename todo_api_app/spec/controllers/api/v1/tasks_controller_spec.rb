require 'rails_helper'

describe Api::V1::TasksController, type: :controller do
  describe 'GET #index' do
    describe 'empty dataset' do
      before { get :index }

      subject { JSON.parse(response.body) }

      it 'responds with 200 status code' do
        expect(response.status).to eq 200
      end

      it 'does not contain task objects' do
        expect(subject['data']).to be_empty
      end
    end

    describe 'response data' do
      let(:task_title) { 'Index task' }

      before do
        create(:task_with_tags, title: task_title, tags_count: 3)
        get :index
      end

      subject { JSON.parse(response.body) }

      it 'contain 1 task object' do
        expect(subject['data'].size).to eq 1
      end

      it 'contain data of correct type' do
        expect(subject['data'].first['type']).to eq('tasks')
      end

      it 'contain task with correct attributes' do
        expect(subject['data'].first['attributes']['title']).to eq(task_title)
      end

      it 'contain tags' do
        expect(subject['data'].first['relationships']['tags']['data'].size).to eq(3)
        expect(subject['included'].size).to eq(3)
      end
    end

    describe 'POST #create' do
      context 'with valid attributes' do
        let(:task_attrs) { attributes_for(:task, title: 'Created task') }

        before do
          post :create, params: build_task_params(title: task_attrs[:title])
        end

        it 'responds with 200 status code' do
          expect(response.status).to eq 200
        end

        it 'saves the task in database' do
          expect(Task.where(title: task_attrs[:title]).count).to eq(1)
        end

        describe 'response data' do
          subject { JSON.parse(response.body) }

          it 'contain created task' do
            expect(subject['data']['attributes']['title']).to eq(task_attrs[:title])
          end
        end
      end

      context 'with invalid attributes' do
        describe 'when title too long' do
          let(:task_attrs) { attributes_for(:task, title: 'Way too long task title '*15) }

          before do
            post :create, params: build_task_params(title: task_attrs[:title])
          end

          it 'responds with 422 status code' do
            expect(response.status).to eq 422
          end

          it 'Does not save the task in database' do
            expect(Task.where(title: task_attrs[:title]).count).to eq(0)
          end

          describe 'response data' do
            subject { JSON.parse(response.body) }

            it 'contain errors on title' do
              expect(subject['errors']).to include('title')
            end
          end
        end

        describe 'when title already exists' do
          let(:same_title) { 'Same title' }

          before do
            create :task, title: same_title
            post :create, params: build_task_params(title: same_title)
          end

          it 'responds with 422 status code' do
            expect(response.status).to eq 422
          end

          it 'does not save to database' do
            expect(Task.where(title: same_title).count).to eq(1)
          end

          describe 'response data' do
            subject { JSON.parse(response.body) }

            it 'contain errors on title' do
              expect(subject['errors']).to include('title')
            end
          end
        end
      end
    end
  end

  describe 'PATCH #update' do
    context 'without tags' do
      let(:target_task) { create(:task, title: 'Target task') }
      let(:task_attrs) { attributes_for(:task, title: 'Updated task') }

      before do
        patch :update, {
          params: build_task_params(
            id: target_task.id,
            type: 'tasks',
            title: task_attrs[:title]
          ).merge(id: target_task.id)
        }
      end

      it 'responds with 200 status code' do
        expect(response.status).to eq 200
      end

      it 'updates the task in database' do
        expect(Task.find(target_task.id).title).to eq(task_attrs[:title])
      end

      describe 'response data' do
        subject { JSON.parse(response.body) }

        it 'contain updated task' do
          expect(subject['data']['attributes']['title']).to eq(task_attrs[:title])
        end
      end
    end

    context 'with tags' do
      let(:target_task) { create(:task, title: 'Target task') }
      let(:target_task_tag) { create(:tag, title: 'Target task tag', tasks: [target_task]) }
      let(:task_attrs) { attributes_for(:task, title: 'Updated task') }
      let(:tags_attrs) { attributes_for_list(:tag, 3) }
      let(:tags_titles) { tags_attrs.map{|i| i[:title]} }

      before do
        target_task_tag
        expect(Task.find(target_task.id).tags.count).to eq(1)
        expect(Task.find(target_task.id).tags).to include(target_task_tag)

        patch :update, {
          params: build_task_params(
            id: target_task.id,
            type: 'tasks',
            title: task_attrs[:title],
            tags: tags_titles,
          ).merge(id: target_task.id)
        }
      end

      it 'responds with 200 status code' do
        expect(response.status).to eq 200
      end

      describe 'database' do
        subject { Task.find(target_task.id) }

        it 'has the task with updated title' do
          expect(subject.title).to eq(task_attrs[:title])
        end

        it 'has the task with updated tags' do
          expect(subject.tags.count).to eq(3)
          expect(subject.tags.map(&:title)).to include(*tags_titles)
          expect(subject.tags).not_to include(target_task_tag)
        end
      end

      describe 'response data' do
        subject { JSON.parse(response.body) }

        it 'contain updated task title' do
          expect(subject['data']['attributes']['title']).to eq(task_attrs[:title])
        end

        it 'contain updated task tags' do
          expect(subject['data']['relationships']['tags']['data'].size).to eq(3)
          expect(subject['included'].size).to eq(3)
        end
      end
    end

    context 'with invalid attributes' do
      let(:target_task) { create(:task, title: 'Target task') }

      describe 'when title too long' do
        let(:task_attrs) { attributes_for(:task, title: 'Way too long task title '*15) }

        before do
          patch :update, {
            params: build_task_params(
              id: target_task.id,
              type: 'tasks',
              title: task_attrs[:title],
            ).merge(id: target_task.id)
          }
        end

        it 'responds with 422 status code' do
          expect(response.status).to eq 422
        end

        it 'Does not changes in database' do
          expect(Task.find(target_task.id).title).not_to eq(task_attrs[:title])
        end

        describe 'response data' do
          subject { JSON.parse(response.body) }

          it 'contain errors on title' do
            expect(subject['errors']).to include('title')
          end
        end
      end

      describe 'when title already exists' do
        let(:task_attrs) { attributes_for(:task) }
        let(:same_title) { task_attrs[:title] }

        before do
          create :task, title: same_title
          patch :update, {
            params: build_task_params(
              id: target_task.id,
              type: 'tasks',
              title: same_title,
            ).merge(id: target_task.id)
          }
        end

        it 'responds with 422 status code' do
          expect(response.status).to eq 422
        end

        it 'does not save to database' do
          expect(Task.where(title: same_title).count).to eq(1)
        end

        describe 'response data' do
          subject { JSON.parse(response.body) }

          it 'contain errors on title' do
            expect(subject['errors']).to include('title')
          end
        end
      end

      describe 'when adding same tag multiple times' do
        let(:tag_attrs) { attributes_for(:tag) }
        let(:tag_title) { tag_attrs[:title] }

        before do
          patch :update, {
            params: build_task_params(
              id: target_task.id,
              type: 'tasks',
              title: target_task.title,
              tags: [tag_title, tag_title],
            ).merge(id: target_task.id)
          }
        end

        it 'responds with 200 status code' do
          expect(response.status).to eq 200
        end

        it 'saves unique tags to database' do
          task = Task.find(target_task.id)
          expect(task.tags.count).to eq(1)
          expect(task.tags.first.title).to eq(tag_title)
        end

        describe 'response data' do
          subject { JSON.parse(response.body) }

          it 'contain unique tags' do
            expect(subject['data']['relationships']['tags']['data'].size).to eq(1)
            expect(subject['included'].size).to eq(1)
          end
        end
      end

      describe 'when passing different routing id and params' do
        let(:wrong_id) { 123456 }

        before do
          patch :update, {
            params: build_task_params(
              id: wrong_id,
              type: 'tasks',
              title: target_task.title,
            ).merge(id: target_task.id)
          }
        end

        it 'responds with 422 status code' do
          expect(response.status).to eq 422
        end

        describe 'response data' do
          subject { JSON.parse(response.body) }

          it 'contain error message' do
            expect(subject['errors']['id']).to eq('ids mismatch error')
          end
        end
      end

      describe 'when attempting to update unexistent task' do
        let(:wrong_id) { 123456 }

        before do
          patch :update, {
            params: build_task_params(
              id: wrong_id,
              type: 'tasks',
              title: target_task.title,
            ).merge(id: wrong_id)
          }
        end

        it 'responds with 422 status code' do
          expect(response.status).to eq 422
        end

        describe 'response data' do
          subject { JSON.parse(response.body) }

          it 'contain error message' do
            expect(subject['errors']['id']).to eq('not found')
          end
        end
      end
    end
  end
end
