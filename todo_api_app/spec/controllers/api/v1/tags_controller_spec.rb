require 'rails_helper'

describe Api::V1::TagsController, type: :controller do
  describe 'GET #index' do
    describe 'empty dataset' do
      before { get :index }

      subject { JSON.parse(response.body) }

      it 'responds with 200 status code' do
        expect(response.status).to eq 200
      end

      it 'does not contain tag objects' do
        expect(subject['data']).to be_empty
      end
    end

    describe 'response data' do
      let(:tag_title) { 'Index tag' }

      before do
        create(:tag, title: tag_title)
        get :index
      end

      subject { JSON.parse(response.body) }

      it 'contain 1 tag object' do
        expect(subject['data'].size).to eq 1
      end

      it 'contain data of correct type' do
        expect(subject['data'].first['type']).to eq('tags')
      end

      it 'contain tag with correct attributes' do
        expect(subject['data'].first['attributes']['title']).to eq(tag_title)
      end
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      let(:tag) { build(:tag, title: 'Created tag') }

      before do
        post :create, params: build_tag_params(title: tag.title)
      end

      it 'responds with 200 status code' do
        expect(response.status).to eq 200
      end

      it 'saves the tag in database' do
        expect(Tag.where(title: tag.title).count).to eq(1)
      end

      describe 'response data' do
        subject { JSON.parse(response.body) }

        it 'contain created tag' do
          expect(subject['data']['attributes']['title']).to eq(tag.title)
        end
      end
    end

    context 'with invalid attributes' do
      describe 'when title too long' do
        let(:tag_attrs) { attributes_for(:tag, title: 'Way too long tag title'*3) }

        before do
          post :create, params: build_tag_params(title: tag_attrs[:title])
        end

        it 'responds with 422 status code' do
          expect(response.status).to eq 422
        end

        it 'does not save to database' do
          expect(Tag.where(title: tag_attrs[:title]).count).to eq(0)
        end

        describe 'response data' do
          subject { JSON.parse(response.body) }

          it 'contain errors on title' do
            expect(subject['errors']).to include('title')
          end
        end
      end

      describe 'when title already exists' do
        let(:same_title) { 'Same title' }

        before do
          create :tag, title: same_title
          post :create, params: build_tag_params(title: same_title)
        end

        it 'responds with 422 status code' do
          expect(response.status).to eq 422
        end

        it 'does not save to database' do
          expect(Tag.where(title: same_title).count).to eq(1)
        end

        describe 'response data' do
          subject { JSON.parse(response.body) }

          it 'contain errors on title' do
            expect(subject['errors']).to include('title')
          end
        end
      end
    end
  end

  describe 'PATCH #update' do
    let(:target_tag) { create(:tag, title: 'Target tag') }

    context 'with valid attributes' do
      let(:tag_attrs) { attributes_for(:tag, title: 'Updated tag') }

      before do
        patch :update, {
          params: build_tag_params(
            id: target_tag.id,
            type: 'tags',
            title: tag_attrs[:title]
          ).merge(id: target_tag.id)
        }
      end

      it 'responds with 200 status code' do
        expect(response.status).to eq 200
      end

      it 'updates the tag in database' do
        expect(Tag.find(target_tag.id).title).to eq(tag_attrs[:title])
      end

      describe 'response data' do
        subject { JSON.parse(response.body) }

        it 'contain updated tag' do
          expect(subject['data']['attributes']['title']).to eq(tag_attrs[:title])
        end
      end
    end

    context 'with invalid attributes' do
      describe 'when title too long' do
        let(:tag_attrs) { attributes_for(:tag, title: 'way too long tag title '*3) }

        before do
          patch :update, {
            params: build_tag_params(
              id: target_tag.id,
              type: 'tags',
              title: tag_attrs[:title]
            ).merge(id: target_tag.id)
          }
        end

        it 'responds with 422 status code' do
          expect(response.status).to eq 422
        end

        it 'does not save changes to database' do
          expect(Tag.where(title: tag_attrs[:title]).count).to eq(0)
        end

        describe 'response data' do
          subject { JSON.parse(response.body) }

          it 'contain errors on title' do
            expect(subject['errors']).to include('title')
          end
        end
      end
    end
  end
end
